package fo.nya.dungeon.algo;

import fo.nya.dungeon.struct.Block;
import fo.nya.dungeon.struct.Dungeon;
import fo.nya.dungeon.util.GraphUtility;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by 0da on 04.09.2023 21:37; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 */
class CellularAutomataTest {
    @RepeatedTest(1357)
    void withoutConnections() {

        Dungeon dungeon = GenerationAlgorithm.CELLULAR_AUTOMATA.generate(ThreadLocalRandom.current(), 20, 20, null, null);

        Assertions.assertThat(GraphUtility.allConnected(dungeon, Block.AIR))
                .isTrue();
    }

    @RepeatedTest(13)
    void withConnections() {

        Dungeon toConnect = new Dungeon(1, 7);

        toConnect.set(0, 0, Block.GROUND);
        toConnect.set(0, 1, Block.AIR);
        toConnect.set(0, 2, Block.GROUND);
        toConnect.set(0, 3, Block.AIR);
        toConnect.set(0, 4, Block.GROUND);
        toConnect.set(0, 5, Block.AIR);
        toConnect.set(0, 6, Block.GROUND);


        Dungeon dungeon = GenerationAlgorithm.CELLULAR_AUTOMATA.generate(ThreadLocalRandom.current(), 5, 7, toConnect, toConnect);

        boolean leftSideEntrance = false;
        boolean rightSideEntrance = false;

        for (int y = 0; y < toConnect.height(); y++) {
            // Right side to Left side
            leftSideEntrance |= toConnect.get(toConnect.width() - 1, y) == Block.AIR
                                && dungeon.get(0, y) == Block.AIR;

            // Left side to Right side
            rightSideEntrance |= toConnect.get(0, y) == Block.AIR
                                 && dungeon.get(dungeon.width() - 1, y) == Block.AIR;

        }

        Assertions.assertThat(leftSideEntrance).isTrue();
        Assertions.assertThat(rightSideEntrance).isTrue();
    }

    @Test void wrongHeight() {
        Dungeon toConnect = new Dungeon(1, 1);

        Assertions.assertThatThrownBy(() -> GenerationAlgorithm.CELLULAR_AUTOMATA.generate(ThreadLocalRandom.current(), 1, 20, null, toConnect))
                .isInstanceOf(IllegalStateException.class);

        Assertions.assertThatThrownBy(() -> GenerationAlgorithm.CELLULAR_AUTOMATA.generate(ThreadLocalRandom.current(), 1, 20, toConnect, null))
                .isInstanceOf(IllegalStateException.class);

    }

}