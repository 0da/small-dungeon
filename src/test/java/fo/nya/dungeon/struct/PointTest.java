package fo.nya.dungeon.struct;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by 0da on 03.09.2023 20:36; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 */
class PointTest {

    @Test
    void withOffset() {

        Point o = new Point(0, 0);

        Assertions.assertThat(o.withOffset(-3, +7))
                .isEqualTo(new Point(-3, +7));
    }

    @RepeatedTest(3)
    void testToString() {
        int x = ThreadLocalRandom.current().nextInt();
        int y = ThreadLocalRandom.current().nextInt();

        Assertions.assertThat(new Point(x, y))
                .hasToString(String.format("(%d, %d)", x, y));
    }
}