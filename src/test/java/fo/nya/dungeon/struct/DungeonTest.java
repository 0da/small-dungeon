package fo.nya.dungeon.struct;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.Condition;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by 0da on 03.09.2023 20:10; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 */
class DungeonTest {

    @Test
    void zeroSize() {
        Assertions.assertThatThrownBy(() -> new Dungeon(0, 1))
                .isInstanceOf(IllegalArgumentException.class);

        Assertions.assertThatThrownBy(() -> new Dungeon(1, 0))
                .isInstanceOf(IllegalArgumentException.class);
    }


    @Test
    void blocks() {

        Dungeon dungeon = new Dungeon(1, 1);

        Assertions.assertThat(dungeon.blocks())
                .containsExactly(Block.AIR);

        dungeon.set(0, 0, Block.GROUND);

        Assertions.assertThat(dungeon.blocks())
                .containsExactly(Block.GROUND);
    }

    @Test
    void widthAndHeight() {
        int w = ThreadLocalRandom.current().nextInt(100, 1000);
        int h = ThreadLocalRandom.current().nextInt(100, 1000);
        Dungeon dungeon = new Dungeon(w, h);

        Assertions.assertThat(dungeon.width())
                .isEqualTo(w);

        Assertions.assertThat(dungeon.height())
                .isEqualTo(h);
    }

    @Test
    void getAndSetThrows() {
        Dungeon dungeon = new Dungeon(5, 5);

        Assertions.assertThatThrownBy(() -> dungeon.set(6, 0, Block.GROUND)).isInstanceOf(IndexOutOfBoundsException.class);
        Assertions.assertThatThrownBy(() -> dungeon.set(0, 6, Block.GROUND)).isInstanceOf(IndexOutOfBoundsException.class);
        Assertions.assertThatThrownBy(() -> dungeon.set(-1, 0, Block.GROUND)).isInstanceOf(IndexOutOfBoundsException.class);
        Assertions.assertThatThrownBy(() -> dungeon.set(0, -1, Block.GROUND)).isInstanceOf(IndexOutOfBoundsException.class);

        Assertions.assertThatThrownBy(() -> dungeon.get(6, 0)).isInstanceOf(IndexOutOfBoundsException.class);
        Assertions.assertThatThrownBy(() -> dungeon.get(0, 6)).isInstanceOf(IndexOutOfBoundsException.class);
        Assertions.assertThatThrownBy(() -> dungeon.get(-1, 0)).isInstanceOf(IndexOutOfBoundsException.class);
        Assertions.assertThatThrownBy(() -> dungeon.get(0, -1)).isInstanceOf(IndexOutOfBoundsException.class);

    }

    @RepeatedTest(11)
    void getAndSet() {
        Dungeon dungeon = new Dungeon(13, 19);


        final int x = ThreadLocalRandom.current().nextInt(dungeon.width());
        final int y = ThreadLocalRandom.current().nextInt(dungeon.height());

        Assertions.assertThat(dungeon.get(x, y))
                .isEqualTo(Block.AIR);

        dungeon.set(x, y, Block.GROUND);

        Assertions.assertThat(dungeon.get(x, y))
                .isEqualTo(Block.GROUND);

        Assertions.assertThat(dungeon.blocks()).areExactly(1, new Condition<>(b -> b == Block.GROUND, "Block of type GROUND."));
    }

    @Test void toStringTest1() {

        //  a a G a a
        //  a G G a a
        //  a a G a a
        //  G a a a a

        Dungeon dungeon = new Dungeon(5, 4);
        dungeon.set(2, 0, Block.GROUND);
        dungeon.set(1, 1, Block.GROUND);
        dungeon.set(2, 1, Block.GROUND);
        dungeon.set(2, 2, Block.GROUND);
        dungeon.set(0, 3, Block.GROUND);

        String expectedToString = "A A G A A\n" +
                                  "A G G A A\n" +
                                  "A A G A A\n" +
                                  "G A A A A";

        Assertions.assertThat(dungeon)
                .hasToString(expectedToString);
    }

    @Test void toStringTest2() {

        //  a a G a
        //  a G G a
        //  a a G a
        //  G a a a
        //  a a a a

        Dungeon dungeon = new Dungeon(4, 5);
        dungeon.set(2, 0, Block.GROUND);
        dungeon.set(1, 1, Block.GROUND);
        dungeon.set(2, 1, Block.GROUND);
        dungeon.set(2, 2, Block.GROUND);
        dungeon.set(0, 3, Block.GROUND);

        String expectedToString = "A A G A\n" +
                                  "A G G A\n" +
                                  "A A G A\n" +
                                  "G A A A\n" +
                                  "A A A A";

        Assertions.assertThat(dungeon)
                .hasToString(expectedToString);
    }
}