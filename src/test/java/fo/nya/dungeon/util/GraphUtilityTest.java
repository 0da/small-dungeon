package fo.nya.dungeon.util;

import fo.nya.dungeon.struct.Block;
import fo.nya.dungeon.struct.Dungeon;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Created by 0da on 03.09.2023 18:26; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 */
class GraphUtilityTest {

    @Test void notConnected() {

        //  G a
        //  a G

        Dungeon dungeon = new Dungeon(2, 2);
        dungeon.set(0, 0, Block.GROUND);
        dungeon.set(1, 1, Block.GROUND);

        Assertions.assertThat(GraphUtility.allConnected(dungeon, Block.AIR))
                .isFalse();
    }

    @Test void allConnected() {

        //  G a
        //  a a

        Dungeon dungeon = new Dungeon(2, 2);
        dungeon.set(0, 0, Block.GROUND);

        Assertions.assertThat(GraphUtility.allConnected(dungeon, Block.AIR))
                .isTrue();
    }

    @Test void sizeOneWithZeroBlocks() {

        //  G

        Dungeon dungeon = new Dungeon(1, 1);
        dungeon.set(0, 0, Block.GROUND);

        Assertions.assertThat(GraphUtility.allConnected(dungeon, Block.AIR))
                .isTrue();
    }

    @Test void sizeOne() {

        //  a

        Dungeon dungeon = new Dungeon(1, 1);

        Assertions.assertThat(GraphUtility.allConnected(dungeon, Block.AIR))
                .isTrue();
    }


    @Test void empty() {

        //  a a a a
        //  a a a a
        //  a a a a
        //  a a a a


        Dungeon dungeon = new Dungeon(4, 4);

        Assertions.assertThat(GraphUtility.allConnected(dungeon, Block.AIR))
                .isTrue();
    }

    @Test void snake() {

        //  a a G a
        //  a G G a
        //  a a G a
        //  G a a a

        Dungeon dungeon = new Dungeon(4, 4);
        dungeon.set(2, 0, Block.GROUND);
        dungeon.set(1, 1, Block.GROUND);
        dungeon.set(2, 1, Block.GROUND);
        dungeon.set(2, 2, Block.GROUND);
        dungeon.set(0, 3, Block.GROUND);

        Assertions.assertThat(GraphUtility.allConnected(dungeon, Block.AIR))
                .isTrue();
    }
}