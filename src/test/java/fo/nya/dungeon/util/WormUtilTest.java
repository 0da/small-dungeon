package fo.nya.dungeon.util;

import fo.nya.dungeon.struct.Block;
import fo.nya.dungeon.struct.Dungeon;
import fo.nya.dungeon.struct.Point;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by 0da on 04.09.2023 0:19; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 */
class WormUtilTest {
    @Test void makeWay() {
        Dungeon dungeon = new Dungeon(19, 6);
        Arrays.fill(dungeon.blocks(), Block.GROUND);

        dungeon.set(18, 5, Block.AIR);

        WormUtil.makeWay(ThreadLocalRandom.current(), dungeon, new Point(0, 0));

        Assertions.assertThat(GraphUtility.allConnected(dungeon, Block.AIR))
                .isTrue();
    }

    @Test void noWay() {
        Dungeon dungeon = new Dungeon(19, 6);
        Arrays.fill(dungeon.blocks(), Block.GROUND);

        WormUtil.makeWay(ThreadLocalRandom.current(), dungeon, new Point(0, 0));

        Assertions.assertThat(dungeon.blocks())
                .containsOnly(Block.AIR);
    }
}