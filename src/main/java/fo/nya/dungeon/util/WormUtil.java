package fo.nya.dungeon.util;

import fo.nya.dungeon.struct.Block;
import fo.nya.dungeon.struct.Dungeon;
import fo.nya.dungeon.struct.Point;

import java.util.*;

/**
 * Created by 0da on 04.09.2023 0:09; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 * <br>
 * Класс утилита для создания путей. Имея изначальную точку алгоритм будет 'прогрызать' себе путь пока не найдет открытое пространство.
 */
public final class WormUtil {

    /**
     * Приватный конструктор т.к. этот класс является утилитой.
     */
    private WormUtil() {
        // Исключение не кидает т.к. в этом нет особо смысла.
    }

    /**
     * Метод для проделывания ходов от входной точки до основного пути. Имея изначальную точку алгоритм будет 'прогрызать' себе путь пока не найдет открытое пространство.
     * В результате алгоритм потенциально может съесть все поле.
     *
     * @param random  генератор случайных чисел для работы алгоритма.
     * @param dungeon подземелье в котором нужно проделать путь.
     * @param point   точка с которой надо начать. В данной точке будет установлен блок воздуха в любом случае.
     */
    public static void makeWay(Random random, Dungeon dungeon, Point point) {

        ArrayList<Direction> directions = new ArrayList<>(Arrays.asList(Direction.values()));

        dungeon.set(point.x, point.y, Block.AIR);

        Deque<Point> path = new LinkedList<>();
        path.addLast(point);

        Set<Point> forbidden = new HashSet<>();

        outer:
        while (true) {
            Point last = path.peekLast();

            if (last == null) break;
            forbidden.add(last);

            Collections.shuffle(directions, random);

            for (Direction dir : directions) {
                if (dir.nextInDiapason(dungeon, last)) {
                    Point candidate = dir.withOffset(last);

                    if (forbidden.contains(candidate)) continue;

                    if (dungeon.get(candidate.x, candidate.y) == Block.GROUND) {
                        dungeon.set(candidate.x, candidate.y, Block.AIR);
                        forbidden.add(candidate);
                        path.add(candidate);
                        continue outer;
                    }

                    return;
                }
            }

            path.pollLast();
        }
    }

    /**
     * Метод проделывает один ход с помощью метода {@link #makeWay(Random, Dungeon, Point)} в текущем подземелье так,
     * чтоб в него можно было зайти из предыдущего.
     *
     * @param random   генератор случайных чисел для работы алгоритма.
     * @param previous предыдущее подземелье, не будет модифицировано.
     * @param current  текущее подземелье, будет модифицировано так, чтоб в него можно было попасть из предыдущего.
     */
    public static void makeTransitionFromPreviousToCurrent(Random random, Dungeon previous, Dungeon current) {
        final int lastXOfPrevious = previous.width() - 1;

        final int oy = random.nextInt();

        for (int y = 0; y < previous.height(); y++) {

            final int ny = Math.abs(oy + y) % previous.height();

            if (previous.get(lastXOfPrevious, ny) == Block.AIR) {
                WormUtil.makeWay(random, current, new Point(0, ny));
                break;
            }
        }
    }

    /**
     * Метод проделывает один ход с помощью метода {@link #makeWay(Random, Dungeon, Point)} в текущем подземелье так,
     * чтоб из него можно было попасть в следующее.
     *
     * @param random  генератор случайных чисел для работы алгоритма.
     * @param current текущее подземелье, будет модифицировано так, чтоб из него можно было попасть из следующее.
     * @param next    следующее подземелье, не будет модифицировано.
     */
    public static void makeTransitionFromCurrentToNext(Random random, Dungeon current, Dungeon next) {
        final int lastXOfMe = current.width() - 1;

        final int oy = random.nextInt();

        for (int y = 0; y < current.height(); y++) {

            final int ny = Math.abs(oy + y) % current.height();

            if (next.get(0, ny) == Block.AIR) {
                WormUtil.makeWay(random, current, new Point(lastXOfMe, ny));
                break;
            }
        }
    }


}
