package fo.nya.dungeon.util;

import fo.nya.dungeon.struct.Dungeon;
import fo.nya.dungeon.struct.Point;

import java.util.Random;

/**
 * Created by 0da on 03.09.2023 21:51; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 * <br>
 * Класс утилита для упрощения работы с 'направлением'.
 */
public enum Direction {
    /**
     * Север/Верх.
     */
    N("W", "E") {
        @Override public Point withOffset(Point point) {return point.withOffset(0, -1);}

        @Override public boolean nextInDiapason(Dungeon dungeon, Point point) {return point.y > 0;}
    },

    /**
     * Юг/Низ.
     */
    S("E", "W") {
        @Override public Point withOffset(Point point) {return point.withOffset(0, +1);}

        @Override public boolean nextInDiapason(Dungeon dungeon, Point point) {return point.y < dungeon.height() - 1;}
    },

    /**
     * Запад/Лево.
     */
    W("S", "N") {
        @Override public Point withOffset(Point point) {return point.withOffset(-1, 0);}

        @Override public boolean nextInDiapason(Dungeon dungeon, Point point) {return point.x > 0;}
    },

    /**
     * Восток/Право.
     */
    E("N", "S") {
        @Override public Point withOffset(Point point) {return point.withOffset(+1, 0);}

        @Override public boolean nextInDiapason(Dungeon dungeon, Point point) {return point.x < dungeon.width() - 1;}
    },

    ;

    /**
     * Кеш всех доступных значений.
     */
    private final static Direction[] DIRECTIONS = values();

    /**
     * Название направления которое будет если 'повернуть' от текущего влево.
     */
    private final String turnLeftName;

    /*
     * Название направления которое будет если 'повернуть' от текущего вправо.
     */
    private final String turnRightName;


    Direction(String turnLeft, String turnRight) {
        this.turnLeftName = turnLeft;
        this.turnRightName = turnRight;
    }

    /**
     * Метод позволяет получить точку на один пункт дальше в направлении которое обозначает этот {@code enum}.
     */
    public abstract Point withOffset(Point point);

    /**
     * Метод проверят факт того, что следующая точка в этом направлении все-еще лежит 'внутри' подземелья.
     */
    public abstract boolean nextInDiapason(Dungeon dungeon, Point point);

    /**
     * Получить значение, которое будет в случае если 'повернуть' влево.
     */
    public Direction turnLeft() {return Direction.valueOf(turnLeftName);}

    /**
     * Получить значение, которое будет в случае если 'повернуть' вправо.
     */
    public Direction turnRight() {return Direction.valueOf(turnRightName);}


    /**
     * Метод возвращает случайно направление из всех доступных.
     */
    public Direction getRandom(Random random) {
        return DIRECTIONS[random.nextInt(DIRECTIONS.length)];
    }
}
