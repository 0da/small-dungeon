package fo.nya.dungeon.util;

import fo.nya.dungeon.struct.Block;
import fo.nya.dungeon.struct.Dungeon;
import fo.nya.dungeon.struct.Point;

import java.util.*;

/**
 * Created by 0da on 03.09.2023 18:19; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 * <br> Класс утилита инкапсулирующий методы для работы с подземельями в качестве графов.
 */
public final class GraphUtility {

    /**
     * Приватный конструктор т.к. этот класс является утилитой.
     */
    private GraphUtility() {
        // Исключение не кидает т.к. в этом нет особо смысла.
    }

    /**
     * Метод Проверяет что все блоки указанного типа связаны между собой строго по горизонтали или вертикали.
     *
     * @param dungeon комната 'подземелья' которую нужно проверить.
     * @param block   тип блока связность которого нужно проверить.
     * @return {@code true} в случае если все блоки типа {@code block} соединены по горизонтали либо по вертикали, иначе {@code false}.
     * В случае если в комнате нет вообще блоков этого типа, то все равно будет считаться что они соединены между собой.
     */
    public static boolean allConnected(Dungeon dungeon, Block block) {

        Point first = null;
        int total = 0;

        for (int x = 0; x < dungeon.width(); x++) {
            for (int y = 0; y < dungeon.height(); y++) {
                if (dungeon.get(x, y) == block) {
                    if (first == null) first = new Point(x, y);
                    total++;
                }
            }
        }

        // В случае если точек 0 или 1, то значит можно сказать что они все соединены.
        if (total < 2) return true;

        // Подобие обхода в ширину:

        Queue<Point> unchecked = new LinkedList<>();
        Set<Point> checked = new HashSet<>();
        unchecked.add(first);

        while (true) {
            Point poll = unchecked.poll();
            if (poll == null) break;

            if (dungeon.get(poll.x, poll.y) != block) {
                continue;
            }

            if (!checked.add(poll)) {
                continue;
            }

            if (poll.x > 0) unchecked.add(poll.withOffset(-1, 0));
            if (poll.y > 0) unchecked.add(poll.withOffset(0, -1));

            if (poll.x < dungeon.width() - 1) unchecked.add(poll.withOffset(+1, 0));
            if (poll.y < dungeon.height() - 1) unchecked.add(poll.withOffset(0, +1));
        }

        return checked.size() == total;
    }

    /**
     * Кривой, косой, не оптимальный алгоритм для соединения всех точек если они спрятались где-то отдельно от общей кучи.
     *
     * @param dungeon подземелье в котором
     * @param block   тип блоков которые нужно соединить.
     */
    public static void connectUnconnected(Dungeon dungeon, Block block) {

        int[][] costs = new int[dungeon.width()][dungeon.height()];

        Queue<Point> points = new LinkedList<>();

        Point root = null;

        outer:
        for (int x = 0; x < dungeon.width(); x++) {
            for (int y = 0; y < dungeon.height(); y++) {
                if (dungeon.get(x, y) == block) {

                    root = new Point(x, y);

                    if (x > 0) points.add(new Point(x - 1, y));
                    if (y > 0) points.add(new Point(x, y - 1));
                    if (x < dungeon.width() - 1) points.add(new Point(x + 1, y));
                    if (y < dungeon.height() - 1) points.add(new Point(x, y + 1));

                    break outer;
                }
            }
        }

        if (root == null) return;

        for (int[] cost : costs) {
            Arrays.fill(cost, dungeon.blocks().length);
        }

        costs[root.x][root.y] = 0;

        int iterations = 1;

        for (int i = 0; i < iterations; i++) {

            boolean mayBeRemoved = i > 0;

            while (true) {
                Point poll = points.poll();
                if (poll == null) break;

                final int currentCost = costs[poll.x][poll.y];
                int minCost = currentCost;

                if (currentCost == 0) continue;

                if (poll.x > 0) minCost = Math.min(costs[poll.x - 1][poll.y], minCost);
                if (poll.y > 0) minCost = Math.min(costs[poll.x][poll.y - 1], minCost);
                if (poll.x < dungeon.width() - 1) minCost = Math.min(costs[poll.x + 1][poll.y], minCost);
                if (poll.y < dungeon.height() - 1) minCost = Math.min(costs[poll.x][poll.y + 1], minCost);


                if (minCost < 0) throw new IndexOutOfBoundsException();

                int actualCost = minCost > 0 || dungeon.get(poll.x, poll.y) != block ? (minCost + 1) : 0;

                if (actualCost == currentCost) continue;

                costs[poll.x][poll.y] = actualCost;

                if (connectAllHelperForBlockReplacement(mayBeRemoved, dungeon, block, costs, points, poll, actualCost, -1, 0))
                    mayBeRemoved = false;

                if (connectAllHelperForBlockReplacement(mayBeRemoved, dungeon, block, costs, points, poll, actualCost, 0, -1))
                    mayBeRemoved = false;

                if (connectAllHelperForBlockReplacement(mayBeRemoved, dungeon, block, costs, points, poll, actualCost, +1, 0))
                    mayBeRemoved = false;

                if (connectAllHelperForBlockReplacement(mayBeRemoved, dungeon, block, costs, points, poll, actualCost, 0, +1))
                    mayBeRemoved = false;
            }

            for (int x = 0; x < dungeon.width(); x++) {
                for (int y = 0; y < dungeon.height(); y++) {
                    if (dungeon.get(x, y) == Block.AIR && costs[x][y] > 0) {
                        costs[x][y] = dungeon.blocks().length;
                        points.add(new Point(x, y));
                    }
                }
            }

            if (!points.isEmpty()) {
                iterations++;
            }
        }

    }

    /**
     * Метод помощник который добавляет точку в очередь на обработку, и ставит на её место блок 'целевой', в случае если текущий блок является 'целевым'.
     */
    private static boolean connectAllHelperForBlockReplacement(boolean replace, Dungeon dungeon, Block block, int[][] costs, Queue<Point> points, Point poll, int actualCost, int dx, int dy) {

        final int tx = poll.x + dx;
        final int ty = poll.y + dy;

        if (tx < 0 || ty < 0 || tx >= dungeon.width() || ty >= dungeon.height()) {
            return false;
        }

        if (replace && actualCost > 0 && dungeon.get(poll.x, poll.y) == block && dungeon.get(tx, ty) != block && costs[tx][ty] < actualCost) {
            dungeon.set(tx, ty, block);
            return true;
        }

        points.add(new Point(tx, ty));
        return false;
    }

}
