package fo.nya.dungeon.struct;

import java.util.Objects;

/**
 * Created by 0da on 03.09.2023 18:20; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 * <br>
 * Структура для хранения координат точки.
 */
public class Point {

    /**
     * Координата X для этой точки.
     */
    public final int x;

    /**
     * Координата Y для этой точки.
     */
    public final int y;

    /**
     * Конструктор для создания новой точки с указанными координатами.
     *
     * @param x координата X для этой точки.
     * @param y координата Y для этой точки.
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Метод создает новую точки с относительным отступом от текущей точки.
     *
     * @param dx отступ по оси X.
     * @param dy отступ по оси Y.
     * @return новая точка с координатами {@code (x + dx, y + dy)}.
     */
    public Point withOffset(int dx, int dy) {
        return new Point(x + dx, y + dy);
    }

    /**
     * Метод проверяет факт того что точка лежит 'внутри' подземелья.
     *
     * @param dungeon подземелье которое нужно проверить.
     * @return {@code true} в случае если точка лежит 'внутри' подземелья, иначе {@code false}.
     */
    public boolean in(Dungeon dungeon) {
        return x >= 0 && x < dungeon.width() && y >= 0 && y < dungeon.height();
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point)) return false;
        Point point = (Point) o;
        return x == point.x && y == point.y;
    }

    @Override public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override public String toString() {
        return "(" + x + ", " + y + ')';
    }
}