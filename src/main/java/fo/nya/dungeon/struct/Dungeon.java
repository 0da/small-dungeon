package fo.nya.dungeon.struct;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by 0da on 03.09.2023 18:20; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 * <br>
 * Структура подземелья, а точнее одной комнаты подземелья. Содержит информацию о том какого размера подземелье, и из каких блоков оно было сделано.
 */
public class Dungeon {
    /**
     * Горизонтальный размер подземелья, должен быть 1 или более.
     */
    private final int width;

    /**
     * Вертикальный размер подземелья, должен быть 1 или более.
     */
    private final int height;

    /**
     * Набор блоков из которых состоит подземелье.
     */
    private final Block[] blocks;

    /**
     * Конструктор для создания нового подземелья необходимой ширины и высоты. Изначально все блоки подземелья заполнены воздухом({@link Block#AIR}).
     *
     * @param width  горизонтальный размер подземелья, должен быть 1 или более.
     * @param height вертикальный размер подземелья, должен быть 1 или более.
     * @throws IllegalArgumentException в случае если {@code width} или {@code height} меньше 1.
     */
    public Dungeon(int width, int height) {

        if (width < 1 || height < 1) {
            throw new IllegalArgumentException("Width and Height must be greater than 0, actual is {w: " + width + ", h: " + height + "}.");
        }

        this.width = width;
        this.height = height;

        this.blocks = new Block[width * height];

        Arrays.fill(blocks, Block.AIR);
    }

    /**
     * Метод возвращает массив в котором хранятся состояния блоков, модификация массива приведет к изменению подземелья.
     */
    public Block[] blocks() {
        return blocks;
    }

    /**
     * Метод возвращает ширину подземелья.
     *
     * @see #width
     */
    public int width() {
        return width;
    }

    /**
     * Метод возвращает высоту подземелья.
     *
     * @see #height
     */
    public int height() {
        return height;
    }

    /**
     * Метод возвращает состояние блока на определенной координате внутри подземелья.
     *
     * @param x горизонтальная координата для получения состояния блока, должна быть в диапазоне {@code [0, width)}.
     * @param y вертикальная координата для получения состояния блока, должна быть в диапазоне {@code [0, height)}.
     * @return Состояние блока на указанной координате, метод всегда не равен {@code null}.
     * @throws IndexOutOfBoundsException в случае если переданные координаты находятся за пределами указанных диапазонов.
     * @see #width()
     * @see #height()
     */
    public Block get(int x, int y) {

        if (x >= 0 && x < width && y >= 0 && y < height) {
            return blocks[width * y + x];
        }

        throw new IndexOutOfBoundsException("Coordinates (x, y) must be in diapason less than width and height respectively and greater or equal to 0, actual is {x: " + x + ", y: " + y + "}.");
    }

    /**
     * Метод устанавливает в указанную позицию переданное состояние блока.
     *
     * @param x     горизонтальная координата для изменения состояния блока, должна быть в диапазоне {@code [0, width)}.
     * @param y     вертикальная координата для изменения состояния блока, должна быть в диапазоне {@code [0, height)}.
     * @param block новое состояние блока с которым нужно ассоциировать координату. Должно быть не {@code null}.
     * @throws IndexOutOfBoundsException в случае если переданные координаты находятся за пределами указанных диапазонов.
     * @throws NullPointerException      в случае если {@code block == null}.
     * @see #width()
     * @see #height()
     */
    public void set(int x, int y, Block block) {

        Objects.requireNonNull(block, "Block must not null.");

        if (x >= 0 && x < width && y >= 0 && y < height) {
            blocks[width * y + x] = block;
            return;
        }

        throw new IndexOutOfBoundsException("Coordinates (x, y) must be in diapason less than width and height respectively and greater or equal to 0, actual is {x: " + x + ", y: " + y + "}.");
    }

    @Override public String toString() {
        StringBuilder builder = new StringBuilder();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {

                builder.append(get(x, y).name().charAt(0));
                if (x + 1 < width) builder.append(" ");

            }
            if (y + 1 < height) builder.append("\n");
        }

        return builder.toString();
    }
}

