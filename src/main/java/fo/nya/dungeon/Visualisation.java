package fo.nya.dungeon;

import fo.nya.dungeon.algo.GenerationAlgorithm;
import fo.nya.dungeon.struct.Block;
import fo.nya.dungeon.struct.Dungeon;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Created by 0da on 03.09.2023 18:23; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 * <br> Простой код для визуализации подземелья.
 */
public class Visualisation extends JPanel {

    private final static GenerationAlgorithm ALGORITHM = GenerationAlgorithm.RANDOM_OF_KNOWN;

    private final static int WIDTH = 70;
    private final static int HEIGHT = 40;

    private final static int SCALE = 20;

    private final static Dungeon NEXT = GenerationAlgorithm.RANDOMIZE_UNTIL_OK.generate(ThreadLocalRandom.current(), WIDTH, HEIGHT, null, null);

    private Dungeon current;
    private Dungeon old;
    private Dungeon oldest;
    private int offset = 0;

    @Override
    public void paint(Graphics g) {
        Graphics2D graphic2d = (Graphics2D) g;
        if (current == null || old == null || oldest == null) return;

        graphic2d.setColor(Color.LIGHT_GRAY);
        for (int x = 0; x < oldest.width(); x++) {
            for (int y = 0; y < oldest.height(); y++) {
                if (oldest.get(x, y) == Block.GROUND) {
                    graphic2d.fillRect(x * SCALE - SCALE * WIDTH + offset, y * SCALE, SCALE, SCALE);
                }
            }
        }

        graphic2d.setColor(Color.GRAY);
        for (int x = 0; x < old.width(); x++) {
            for (int y = 0; y < old.height(); y++) {
                if (old.get(x, y) == Block.GROUND) {
                    graphic2d.fillRect(x * SCALE + offset, y * SCALE, SCALE, SCALE);
                }
            }
        }

        graphic2d.setColor(Color.DARK_GRAY);
        for (int x = 0; x < current.width(); x++) {
            for (int y = 0; y < current.height(); y++) {
                if (current.get(x, y) == Block.GROUND) {
                    graphic2d.fillRect(WIDTH * SCALE + x * SCALE + offset, y * SCALE, SCALE, SCALE);
                }
            }
        }
    }

    private void update(boolean force) {

        offset -= 10;

        if (offset <= 0 || force) {

            offset = SCALE * WIDTH;

            oldest = old;
            old = current;
            current = ALGORITHM.generate(ThreadLocalRandom.current(), WIDTH, HEIGHT, old, NEXT);
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Dungeons");
        Visualisation visualisation = new Visualisation();

        visualisation.setPreferredSize(new Dimension(WIDTH * SCALE * 2, HEIGHT * SCALE));
        frame.add(visualisation);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.pack();
        frame.setLocationRelativeTo(null);

        visualisation.update(true);
        visualisation.update(true);
        visualisation.update(true);
        frame.repaint();

        Executors.newScheduledThreadPool(1, r -> new Thread(r) {{setDaemon(true);}})
                .scheduleAtFixedRate(() -> {
                    visualisation.update(false);
                    frame.repaint();
                }, 0, 16, TimeUnit.MILLISECONDS);
    }
}