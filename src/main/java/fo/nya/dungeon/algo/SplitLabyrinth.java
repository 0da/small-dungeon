package fo.nya.dungeon.algo;

import fo.nya.dungeon.struct.Block;
import fo.nya.dungeon.struct.Dungeon;
import fo.nya.dungeon.struct.Point;
import fo.nya.dungeon.util.Direction;
import fo.nya.dungeon.util.WormUtil;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

/**
 * Created by 0da on 04.09.2023 1:34; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 * <br>
 * Генератор подземелья алгоритм которого заключается в том, что поле делиться пополам по горизонтали, на случайные половины,
 * затем эти половины делятся по вертикали, затем по горизонтали и т.д. до тех пор пока не останется места.
 */
public class SplitLabyrinth implements GenerationAlgorithm {

    /**
     * Класс утилита для построения лабиринта.
     */
    private static class Zone {

        /**
         * Координата X прямоугольника в котором должна располагаться черта.
         */
        final int x;


        /**
         * Координата Y прямоугольника в котором должна располагаться черта.
         */
        final int y;

        /**
         * Гирина прямоугольника в котором должна располагаться черта.
         */
        final int width;

        /**
         * Высота прямоугольника в котором должна располагаться черта.
         */
        final int height;

        /**
         * Флаг того что черта должна быть горизонтальной({@code true}), иначе вертикальной({@code false}).
         */
        final boolean horizontal;

        /**
         * Направление с которой черта может касаться с другой чертой.
         */
        final Direction touchDirection;

        public Zone(int x, int y, int width, int height, boolean horizontal, Direction touchDirection) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.horizontal = horizontal;
            this.touchDirection = touchDirection;
        }
    }

    /**
     * {@inheritDoc}
     * <br>
     * В данном случае алгоритм которого заключается в том, что поле делиться пополам по горизонтали, на случайные половины,
     * затем эти половины делятся по вертикали, затем по горизонтали и т.д. до тех пор пока не останется места.
     *
     * @param random   генератор случайных чисел который будет использован для генерации комнаты.
     * @param width    требуемая ширина комнаты.
     * @param height   требуемая высота комнаты.
     * @param previous комната идет перед той которая будет сгенерирована, может быть {@code null}. В случае если не {@code null} то новая комната будет совместима с {@code previous}.
     * @param next     комната идет после той которая будет сгенерирована, может быть {@code null}. В случае если не {@code null} то новая комната будет совместима с {@code next}.
     * @return новая комната подземелья соответсвующая указанным параметрам.
     * @throws IllegalStateException в случае если высота не совпадает между новым, предыдущим или следующей комнатами.
     * @throws NullPointerException  в случае если {@code random == null}.
     * @see Dungeon
     */
    @Override public Dungeon generate(Random random, int width, int height, Dungeon previous, Dungeon next) {
        check(random, height, previous, next);

        Dungeon dungeon = new Dungeon(width, height);

        Arrays.fill(dungeon.blocks(), Block.GROUND);

        Queue<Zone> queue = new LinkedList<>();
        queue.add(new Zone(0, 0, width, height, random.nextBoolean(), null));

        while (true) {
            Zone zone = queue.poll();
            if (zone == null) break;

            if (zone.width - zone.x < 1) continue;
            if (zone.height - zone.y < 1) continue;

            int x = zone.x + random.nextInt(zone.width - zone.x);
            int y = zone.y + random.nextInt(zone.height - zone.y);

            dungeon.set(x, y, Block.AIR);

            Direction a;
            Direction b;

            // Последующее зоны определяются не совсем так как должно быть, но именно такой отступ даёт нормально выглядящий результат.
            if (zone.horizontal) {
                a = Direction.W;
                b = Direction.E;

                //   0 1 2 3 4 5 6 7
                // 0
                // 1
                // 2
                // 3
                // 4 - - - - - - - -
                // 5
                // 6

                // x:0, y:0, w:6, h:7
                // ry = 4
                // x:0, y:0, w:6, h:5 (ry+1=4+1)
                // x:0, y:5(ry+1=4+1), w:6, h:7
                queue.add(new Zone(zone.x, zone.y, zone.width, y - 1, false, Direction.S));
                queue.add(new Zone(zone.x, y + 2, zone.width, zone.height, false, Direction.N));

            } else {
                a = Direction.N;
                b = Direction.S;
                queue.add(new Zone(zone.x, zone.y, x - 1, zone.height, true, Direction.E));
                queue.add(new Zone(x + 2, zone.y, zone.width, zone.height, true, Direction.W));
            }

            Point pa = new Point(x, y);
            Point pb = pa;

            do {
                pa = a.withOffset(pa);
            } while (fillPoint(dungeon, pa, a, a == zone.touchDirection));

            do {
                pb = b.withOffset(pb);
            } while (fillPoint(dungeon, pb, b, b == zone.touchDirection));
        }

        if (previous != null) {
            WormUtil.makeTransitionFromPreviousToCurrent(random, previous, dungeon);
        }

        if (next != null) {
            WormUtil.makeTransitionFromCurrentToNext(random, dungeon, next);
        }

        return dungeon;
    }

    /**
     * Выделенный кусок кода, с проверкой о возможности поставить воздух в конкретную точку.
     */
    private boolean fillPoint(Dungeon dungeon, Point pb, Direction dir, boolean touch) {

        boolean in = pb.in(dungeon);

        Point point = dir.withOffset(pb);
        if (!touch && point.in(dungeon) && (dungeon.get(point.x, point.y) == Block.AIR)) {
            return false;
        }

        if (in && dungeon.get(pb.x, pb.y) == Block.GROUND) {
            dungeon.set(pb.x, pb.y, Block.AIR);
            return true;
        }

        return false;
    }
}
