package fo.nya.dungeon.algo;

import fo.nya.dungeon.struct.Block;
import fo.nya.dungeon.struct.Dungeon;
import fo.nya.dungeon.util.GraphUtility;
import fo.nya.dungeon.util.WormUtil;

import java.util.Random;

/**
 * Created by 0da on 04.09.2023 3:37; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 * <br>
 * Алгоритм генерации подземелья на основе клеточного автомата.
 */
public class CellularAutomata implements GenerationAlgorithm {

    /**
     * Количество итераций для клеточного автомата.
     */
    private final int iterations;

    /**
     * Вероятность того что точка изначально будет заполнена блоком {@link Block#GROUND}.
     */
    private final double initialProbability;

    /**
     * Количество соседних клеток при котором клетка будет оставаться 'живой'.
     */
    private final int liveUntil;

    /**
     * Количество клеток при котором клетка воздуха будет превращаться в клетку стены.
     */
    private final int birthOn;

    /**
     * Конструктор создает новый состояние алгоритма с указанными параметрами.
     *
     * @param iterations         количество итераций для клеточного автомата.
     * @param initialProbability вероятность того что точка изначально будет заполнена блоком {@link Block#GROUND}.
     * @param liveUntil          количество соседних клеток при котором клетка будет оставаться 'живой'.
     * @param birthOn            количество клеток при котором клетка воздуха будет превращаться в клетку стены.
     */
    public CellularAutomata(int iterations, double initialProbability, int liveUntil, int birthOn) {
        this.iterations = iterations;
        this.initialProbability = initialProbability;
        this.liveUntil = liveUntil;
        this.birthOn = birthOn;
    }

    /**
     * В данном случае алгоритм заполняет подземелье случайными точками, а затем использует клеточный автомат, для формирования чего-то что выглядит как пещера.
     *
     * @param random   генератор случайных чисел который будет использован для генерации комнаты.
     * @param width    требуемая ширина комнаты.
     * @param height   требуемая высота комнаты.
     * @param previous комната идет перед той которая будет сгенерирована, может быть {@code null}. В случае если не {@code null} то новая комната будет совместима с {@code previous}.
     * @param next     комната идет после той которая будет сгенерирована, может быть {@code null}. В случае если не {@code null} то новая комната будет совместима с {@code next}.
     * @return новая комната подземелья соответсвующая указанным параметрам.
     * @throws IllegalStateException в случае если высота не совпадает между новым, предыдущим или следующей комнатами.
     * @throws NullPointerException  в случае если {@code random == null}.
     * @see Dungeon
     */
    @Override public Dungeon generate(Random random, int width, int height, Dungeon previous, Dungeon next) {
        check(random, height, previous, next);

        Dungeon dungeon = new Dungeon(width, height);

        Dungeon buffer = new Dungeon(width, height);

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                dungeon.set(x, y, initialProbability > random.nextGaussian() ? Block.AIR : Block.GROUND);
            }
        }

        Block[] a = dungeon.blocks();
        Block[] b = buffer.blocks();

        for (int i = 0; i < iterations; i++) {
            System.arraycopy(a, 0, b, 0, a.length);

            for (int x = 0; x < dungeon.width(); x++) {
                for (int y = 0; y < dungeon.height(); y++) {
                    int neighbours = countAliveNeighbours(buffer, x, y);

                    if (buffer.get(x, y) == Block.GROUND) {
                        if (neighbours < liveUntil) {
                            dungeon.set(x, y, Block.AIR);
                        } else {
                            dungeon.set(x, y, Block.GROUND);
                        }
                    } else {
                        if (neighbours >= birthOn) {
                            dungeon.set(x, y, Block.GROUND);

                        } else {
                            dungeon.set(x, y, Block.AIR);
                        }
                    }
                }
            }
        }

        GraphUtility.connectUnconnected(dungeon, Block.AIR);

        if (previous != null) {
            WormUtil.makeTransitionFromPreviousToCurrent(random, previous, dungeon);
        }

        if (next != null) {
            WormUtil.makeTransitionFromCurrentToNext(random, dungeon, next);
        }

        return dungeon;
    }

    /**
     * Метод-помощник для подсчета живых соседей, в случае если клетка уходит ща границу подземелья, то такая клетка считается 'живой'.
     */
    public int countAliveNeighbours(Dungeon dungeon, int x, int y) {
        int count = 0;

        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {

                if (i == 0 && j == 0) continue;

                final int nx = x + i;
                final int ny = y + j;

                if (nx < 0 || ny < 0 || nx >= dungeon.width() || ny >= dungeon.height()) {
                    count = count + 1;
                } else if (dungeon.get(nx, ny) == Block.GROUND) {
                    count = count + 1;
                }
            }
        }

        return count;
    }
}
