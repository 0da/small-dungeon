package fo.nya.dungeon.algo;

import fo.nya.dungeon.struct.Block;
import fo.nya.dungeon.struct.Dungeon;
import fo.nya.dungeon.struct.Point;
import fo.nya.dungeon.util.Direction;
import fo.nya.dungeon.util.WormUtil;

import java.util.*;

/**
 * Created by 0da on 03.09.2023 21:51; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 * <br>
 * Генератор создает подземелье в виде лабиринта, алгоритм рисует 'червячка' пока не упрется в тупик, когда 'червячок'
 * упирается в тупик, лабиринт строится из последней доступной клетки для роста.
 * Последним шагом выходы и входы 'подсоединяются' к готовому лабиринту.
 */
public class BackpropagationLabyrinth implements GenerationAlgorithm {

    /**
     * Небольшая настройка для генерации лабиринта, в случае если этот флаг {@code true},
     * то генератор будет избегать ситуаций когда блоки воздуха касаются друг друга по диагонали.
     */
    private final boolean withCorners;

    /**
     * Конструктор создает новый генератор с соответствующими параметрами.
     *
     * @param withCorners в случае если этот флаг {@code true}, то генератор будет избегать ситуаций когда блоки воздуха касаются друг друга по диагонали.
     */
    public BackpropagationLabyrinth(boolean withCorners) {this.withCorners = withCorners;}


    /**
     * {@inheritDoc}
     * <br>
     * В данном случае алгоритм рисует 'червячка' пока не упрется в тупик, когда 'червячок'
     * упирается в тупик, лабиринт строится из последней доступной клетки для роста.
     * Последним шагом выходы и входы 'подсоединяются' к готовому лабиринту.
     *
     * @param random   генератор случайных чисел который будет использован для генерации комнаты.
     * @param width    требуемая ширина комнаты.
     * @param height   требуемая высота комнаты.
     * @param previous комната идет перед той которая будет сгенерирована, может быть {@code null}. В случае если не {@code null} то новая комната будет совместима с {@code previous}.
     * @param next     комната идет после той которая будет сгенерирована, может быть {@code null}. В случае если не {@code null} то новая комната будет совместима с {@code next}.
     * @return новая комната подземелья соответсвующая указанным параметрам.
     * @throws IllegalStateException в случае если высота не совпадает между новым, предыдущим или следующей комнатами.
     * @throws NullPointerException  в случае если {@code random == null}.
     * @see Dungeon
     */
    @Override public Dungeon generate(Random random, int width, int height, Dungeon previous, Dungeon next) {
        check(random, height, previous, next);

        Dungeon dungeon = new Dungeon(width, height);

        Arrays.fill(dungeon.blocks(), Block.GROUND);

        Deque<Point> path = new LinkedList<>();

        Point start = new Point(random.nextInt(width), random.nextInt(height));
        path.add(start);
        dungeon.set(start.x, start.y, Block.AIR);

        ArrayList<Direction> directions = new ArrayList<>(Arrays.asList(Direction.values()));

        outer:
        while (true) {
            Point point = path.peekLast();

            if (point == null) break;

            Collections.shuffle(directions, random);

            for (Direction dir : directions) {
                if (dir.nextInDiapason(dungeon, point)) {
                    Point candidate = dir.withOffset(point);
                    if (dungeon.get(candidate.x, candidate.y) == Block.GROUND) {

                        if (dir.nextInDiapason(dungeon, candidate)) {
                            Point forward = dir.withOffset(candidate);
                            if (dungeon.get(forward.x, forward.y) == Block.AIR) continue;

                            if (withCorners && checkLeftAndRight(dungeon, dir, forward)) continue;
                        }

                        if (checkLeftAndRight(dungeon, dir, candidate)) continue;

                        dungeon.set(candidate.x, candidate.y, Block.AIR);
                        path.add(candidate);
                        continue outer;
                    }
                }
            }

            path.pollLast();

        }

        if (previous != null) {
            WormUtil.makeTransitionFromPreviousToCurrent(random, previous, dungeon);
        }

        if (next != null) {
            WormUtil.makeTransitionFromCurrentToNext(random, dungeon, next);
        }

        return dungeon;
    }

    /**
     * Метод для проверки того клетки в соседних точках(перпендикулярно направлению) являются 'воздухом'.
     */
    private boolean checkLeftAndRight(Dungeon dungeon, Direction dir, Point forward) {
        Direction turnLeft = dir.turnLeft();
        if (turnLeft.nextInDiapason(dungeon, forward)) {
            Point leftCorner = turnLeft.withOffset(forward);
            if (dungeon.get(leftCorner.x, leftCorner.y) == Block.AIR) return true;
        }

        Direction turnRight = dir.turnRight();
        if (turnRight.nextInDiapason(dungeon, forward)) {
            Point rightCorner = turnRight.withOffset(forward);
            return dungeon.get(rightCorner.x, rightCorner.y) == Block.AIR;
        }
        return false;
    }
}
