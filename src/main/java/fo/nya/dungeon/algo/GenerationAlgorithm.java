package fo.nya.dungeon.algo;

import fo.nya.dungeon.struct.Dungeon;

import java.util.Objects;
import java.util.Random;

/**
 * Created by 0da on 03.09.2023 19:27; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 * <br>
 * Базовый интерфейс для алгоритма генерации подземелья.
 */
public interface GenerationAlgorithm {

    /**
     * Статически доступ к алгоритму {@link RandomizeUntilOk} с параметрами конструктора {@code RandomizeUntilOk(0.40, 0.40)}.
     */
    RandomizeUntilOk RANDOMIZE_UNTIL_OK = new RandomizeUntilOk(0.80, 0.40);

    /**
     * Статически доступ к алгоритму {@link BackpropagationLabyrinth} с параметрами конструктора {@code BackpropagationLabyrinth(true)}.
     */
    BackpropagationLabyrinth BACKPROPAGATION_LABYRINTH_WITH_CORNERS = new BackpropagationLabyrinth(true);

    /**
     * Статически доступ к алгоритму {@link BackpropagationLabyrinth} с параметрами конструктора {@code BackpropagationLabyrinth(false)}.
     */
    BackpropagationLabyrinth BACKPROPAGATION_LABYRINTH_WITHOUT_CORNERS = new BackpropagationLabyrinth(false);

    /**
     * Статически доступ к алгоритму {@link SplitLabyrinth}.
     */
    SplitLabyrinth SPLIT_LABYRINTH = new SplitLabyrinth();

    /**
     * Статически доступ к алгоритму {@link CellularAutomata} с параметрами конструктора {@code CellularAutomata(7, 0.35, 3, 5)}.
     */
    CellularAutomata CELLULAR_AUTOMATA = new CellularAutomata(7, 0.35, 3, 5);

    /**
     * Алгоритм выбирает случайный из уже известных.
     */
    GenerationAlgorithm RANDOM_OF_KNOWN = (random, width, height, previous, next) -> {
        switch (random.nextInt(7)) {
            default:
            case 0:
                return RANDOMIZE_UNTIL_OK.generate(random, width, height, previous, next);
            case 1:
                return BACKPROPAGATION_LABYRINTH_WITH_CORNERS.generate(random, width, height, previous, next);
            case 2:
                return BACKPROPAGATION_LABYRINTH_WITHOUT_CORNERS.generate(random, width, height, previous, next);
            case 3:
            case 4:
                return SPLIT_LABYRINTH.generate(random, width, height, previous, next);
            case 5:
            case 6:
                return CELLULAR_AUTOMATA.generate(random, width, height, previous, next);

        }
    };

    /**
     * Метод генерирует новое подземелье указанного размера, опционально совместимого с предыдущей комнатой и/или следующей.
     *
     * @param random   генератор случайных чисел который будет использован для генерации комнаты.
     * @param width    требуемая ширина комнаты.
     * @param height   требуемая высота комнаты.
     * @param previous комната идет перед той которая будет сгенерирована, может быть {@code null}. В случае если не {@code null} то новая комната будет совместима с {@code previous}.
     * @param next     комната идет после той которая будет сгенерирована, может быть {@code null}. В случае если не {@code null} то новая комната будет совместима с {@code next}.
     * @return новая комната подземелья соответсвующая указанным параметрам.
     */
    Dungeon generate(Random random, int width, int height, Dungeon previous, Dungeon next);

    /**
     * Метод для часто проверки параметров которая подходит под многие алгоритмы,
     *
     * @param random   генератор случайных чисел который будет использован для генерации комнаты.
     * @param height   требуемая высота комнаты.
     * @param previous комната идет перед той которая будет сгенерирована, может быть {@code null}. В случае если не {@code null} то новая комната будет совместима с {@code previous}.
     * @param next     комната идет после той которая будет сгенерирована, может быть {@code null}. В случае если не {@code null} то новая комната будет совместима с {@code next}.
     * @throws IllegalStateException в случае если высота не совпадает между новым, предыдущим или следующей комнатами.
     * @throws NullPointerException  в случае если {@code random == null}.
     */
    default void check(Random random, int height, Dungeon previous, Dungeon next) {
        Objects.requireNonNull(random, "[random] argument must be non null.");

        if (previous != null && previous.height() != height) {
            throw new IllegalStateException("Height of previous Dungeon must be " + height + " but actual is: " + previous.height() + ".");
        }

        if (next != null && next.height() != height) {
            throw new IllegalStateException("Height of previous Dungeon must be " + height + " but actual is: " + next.height() + ".");
        }
    }
}
