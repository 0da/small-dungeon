package fo.nya.dungeon.algo;


import fo.nya.dungeon.struct.Block;
import fo.nya.dungeon.struct.Dungeon;
import fo.nya.dungeon.util.GraphUtility;
import fo.nya.dungeon.util.WormUtil;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by 0da on 03.09.2023 18:23; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 * <br>
 * Простой алгоритм создания комнаты, заключается в том что изначально комната заполняется в случайном порядке воздухом либо стенами,
 * затем случайные блоки в комнате превращаются в воздух до тех пор, пока комната не будет соответствовать условиям.
 *
 * @see fo.nya.dungeon.algo.GenerationAlgorithm
 */
public class RandomizeUntilOk implements GenerationAlgorithm {

    /**
     * Вероятность того что при изначальном заполнении будет сгенерирован блок воздуха, а не земли.
     * Имеет смысл только если находится в диапазоне от 0, до 1. Чем больше, тем более вероятно что сгенерируется воздух.
     */
    private final double initialProbabilityOfAir;

    /**
     * Вероятность того что при блок земли будет заменен на воздух в случае если комната не соответствует условиям.
     * Имеет смысл только если находится в диапазоне от 0, до 1. Чем больше, тем более вероятно что блок будет заменен на воздух.
     */
    private final double probabilityOfGroundRemoval;

    public RandomizeUntilOk(double initialProbabilityOfAir, double probabilityOfGroundRemoval) {
        this.initialProbabilityOfAir = initialProbabilityOfAir;
        this.probabilityOfGroundRemoval = probabilityOfGroundRemoval;
    }

    /**
     * {@inheritDoc}
     * <br>
     * В данном случае алгоритм заключается в том что изначально комната заполняется в случайном порядке воздухом либо стенами,
     * затем случайные блоки в комнате превращаются в воздух до тех пор, пока комната не будет соответствовать условиям.
     *
     * @param random   генератор случайных чисел который будет использован для генерации комнаты.
     * @param width    требуемая ширина комнаты.
     * @param height   требуемая высота комнаты.
     * @param previous комната идет перед той которая будет сгенерирована, может быть {@code null}. В случае если не {@code null} то новая комната будет совместима с {@code previous}.
     * @param next     комната идет после той которая будет сгенерирована, может быть {@code null}. В случае если не {@code null} то новая комната будет совместима с {@code next}.
     * @return новая комната подземелья соответсвующая указанным параметрам.
     * @throws IllegalStateException в случае если высота не совпадает между новым, предыдущим или следующей комнатами.
     * @throws NullPointerException  в случае если {@code random == null}.
     * @see Dungeon
     */
    @Override public Dungeon generate(Random random, int width, int height, Dungeon previous, Dungeon next) {

        check(random, height, previous, next);

        Dungeon dungeon = new Dungeon(width, height);

        Arrays.setAll(dungeon.blocks(), value -> random.nextBoolean() ? Block.AIR : Block.GROUND);

        dungeon.set(random.nextInt(width), ThreadLocalRandom.current().nextInt(height), Block.AIR);

        GraphUtility.connectUnconnected(dungeon, Block.AIR);

        if (previous != null) {
            WormUtil.makeTransitionFromPreviousToCurrent(random, previous, dungeon);
        }

        if (next != null) {
            WormUtil.makeTransitionFromCurrentToNext(random, dungeon, next);
        }

        return dungeon;
    }
}
